import algorithm.GameAlgorithm;
import map.GameMap;
import substance.utils.SubstanceBuilder;
import substance.utils.SubstanceTypesHolder;
import org.junit.Test;
import substance.utils.SubstanceClassGetter;

import static org.junit.Assert.assertEquals;

public class TestSubstanceTest {
    @Test
    public void test() {
        String testPythonScriptPath = ClassLoader.getSystemClassLoader().getResource("TestSubstance.py").getPath();

        Class<?> testSubstanceClass = SubstanceClassGetter.get(testPythonScriptPath);

        System.err.println("Adding substance to holder, it have to be instantiated...");
        SubstanceTypesHolder.add(testSubstanceClass);

        System.err.println("Iterating through substance holder:");

        for (SubstanceTypesHolder.Category category : SubstanceTypesHolder.getCategories()) {
            System.out.println("\t" + category.getName());
            for (String substanceName : category) {
                System.out.println("\t\t" + substanceName);
            }
        }

        GameMap map = new GameMap(1, 2);

        map.fillWith(testSubstanceClass);

        map.get(0, 0).setTemperature(50);

        System.err.println("Iterating simulation...");
        for (int i = 0; i < 5; ++i) {
            System.err.println("Iteration: " + i);
            GameAlgorithm.iterateSimulation(map);
        }
        System.err.println("Iterating finished.");

        assertEquals("Temperature output is invalid.",
                50,
                (map.get(0, 0).getTemperature() + map.get(0, 1).getTemperature()));
    }
}

from substance import SubstanceBase

class TestSubstance(SubstanceBase):
    def __init__(self):
        print "I was created! Hello world from python!"
        print "My name is " + self.getName() + "."
        print "I'm located in " + self.getCategoryName() + "."

    def sayHi(self):
        return "Hi!"

    def getName(self):
        return "TestSubstance"

    def getCategoryName(self):
        return "TestCategory"

    def getDescription(self):
        return "A substance for testing purposes."

    def getRedHue(self):
        return 0.9

    def getGreenHue(self):
        return 0.6

    def getBlueHue(self):
        return 0.4

    def getDensity(self):
        return 0.0

    def isStatic(self):
        return False

    def getThermalConductivity(self):
        return 1.0

    def getFriction(self):
        return 0.0

    def react(self, selfAsParticle, surroundingParticles):
        print "I'm checking myself!"
        print "My temperature is equal to " + str(self.getTemperature()) + " at the moment."
        print "My coordinates are:"
        print "x: " + str(selfAsParticle.getCoordinates().x)
        print "y: " + str(selfAsParticle.getCoordinates().y)

        if not surroundingParticles.isEmpty():
            particle = surroundingParticles.get(0)
            substance = particle.asSubstance()

            print "I'm reacting with " + substance.getName()

            if isinstance(substance, TestSubstance):
                substance.__class__ = TestSubstance
                print "It's just like me, let's ask it so say hi!"
                print "(other substance): " + substance.sayHi()
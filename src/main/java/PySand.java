import gui.GeneralSettingsWindow;
import gui.SubstanceTypeSelectorWindow;
import substance.utils.SubstanceTypesHolder;
import substance.utils.SubstanceClassGetter;

import java.awt.*;

public class PySand {
    public static void main(String[] args) {
        String testPythonScriptPath;

        testPythonScriptPath= "/home/taiga/Projekty/pysand/src/main/resources/TestSubstance.py";

        Class<?> testSubstanceClass = SubstanceClassGetter.get(testPythonScriptPath);

        System.err.println("Adding substance to holder, it have to be instantiated...");
        SubstanceTypesHolder.add(testSubstanceClass);

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                SubstanceTypeSelectorWindow window = new SubstanceTypeSelectorWindow();
                GeneralSettingsWindow settingsWindow = new GeneralSettingsWindow();
            }
        });
    }
}

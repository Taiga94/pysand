package map;

import substance.SubstanceBase;
import substance.utils.SubstanceBuilder;
import substance.utils.SubstanceTypesHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to hold all the substance particles in a matrix.
 *
 * @see SubstanceBase
 */

public class GameMap {
    private SubstanceBase[][] fields;
    private int width;
    private int height;

    /**
     * Constructor that creates a new map with null values in it.
     */

    public GameMap(int width, int height) {
        fields = new SubstanceBase[width][height];
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Fills map with particles of given type.
     */

    public void fillWith(Class<?> substanceType) {
        for (int x = 0; x < width; ++x)
            for (int y = 0; y < height; ++y)
                fields[x][y] = SubstanceBuilder.build(substanceType);
    }

    /**
     * Creates a new instance of {@link Particle} object.
     *
     * @deprecated particle coordinates can be invalid but
     * even then a object will be created.
     */

    public Particle createParticle(SubstanceBase substance, int x, int y) {
        return new Particle(substance, x, y);
    }

    /**
     * Swaps two particles on map.
     * Horizontal coordinates have to be values in range {@code <0, map width - 1>}.
     * Vertical coordinates have to be values in range {@code <0, map height - 1>}.
     *
     * @param firstX horizontal coordinate of first particle.
     * @param firstY vertical coordinate of first particle.
     * @param secondX horizontal coordinate of second particle.
     * @param secondY vertical coordinate of second particle.
     */

    public void swapFields(int firstX, int firstY, int secondX, int secondY) {
        checkCoordinates(firstX, firstY);
        checkCoordinates(secondX, secondY);

        SubstanceBase temp = get(firstX, firstY);

        fields[firstX][firstY] = fields[secondX][secondY];
        fields[secondX][secondY] = temp;
    }

    /**
     * Horizontal coordinate have to be value in range {@code <0, map width - 1>}.
     * Vertical coordinate have to be value in range {@code <0, map height - 1>}.
     *
     * @param x horizontal coordinate of the particle.
     * @param y vertical coordinate of the particle.
     * @return a new instance of {@link Particle} if coordinates are valid or else null.
     */

    public Particle getParticle(int x, int y) {
        return new Particle(get(x, y), x, y);
    }

    /**
     * Makes iteration through fields surrounding given particle easier for
     * {@link algorithm.GameAlgorithm}.
     *
     * If a map is properly initialized with {@link GameMap#fillWith(Class)},
     * returned list will not contain any null values.
     *
     * @param particle that surrounding fields are returned for.
     * @return a list of fields around a particle.
     */

    public List<Particle> getParticlesAround(Particle particle) {
        return getParticlesAround(particle.coordinates.x, particle.coordinates.y);
    }

    /**
     * Makes iteration through fields surrounding given particle easier for
     * {@link algorithm.GameAlgorithm}.
     *
     * If a map is properly initialized with {@link GameMap#fillWith(Class)},
     * returned list will not contain any null values.
     *
     * @return a list of fields around a particle.
     */

    public List<Particle> getParticlesAround(int x, int y) {
        checkCoordinates(x, y);

        ArrayList<Particle> particlesAround = new ArrayList<Particle>();

        if (y > 0) {
            particlesAround.add(getParticle(x, y - 1));

            if (x > 0)
                particlesAround.add(getParticle(x - 1, y - 1));

            if (x < width - 1)
                particlesAround.add(getParticle(x + 1, y - 1));
        }

        if (x > 0)
            particlesAround.add(getParticle(x - 1, y));

        if (x < width - 1)
            particlesAround.add(getParticle(x + 1, y));

        if (y < height - 1) {
            particlesAround.add(getParticle(x, y + 1));

            if (x > 0)
                particlesAround.add(getParticle(x - 1, y + 1));

            if (x < width - 1)
                particlesAround.add(getParticle(x + 1, y + 1));
        }

        return particlesAround;
    }

    /**
     * Horizontal coordinate have to be value in range {@code <0, map width - 1>}.
     * Vertical coordinate have to be value in range {@code <0, map height - 1>}.
     *
     * @param x horizontal coordinate of the particle.
     * @param y vertical coordinate of the particle.
     * @return a substance if coordinates are valid or else null.
     */

    public SubstanceBase get(int x, int y) {
        try {
            return fields[x][y];
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Replaces a particle on given position with new instance of a substance
     * described by parameters.
     *
     * Horizontal coordinate have to be value in range {@code <0, map width - 1>}.
     * Vertical coordinate have to be value in range {@code <0, map height - 1>}.
     *
     * @param x horizontal coordinate of the particle.
     * @param y vertical coordinate of the particle.
     */

    public void set(String categoryName, String substanceName, int x, int y) {
        Class<?> toSet = SubstanceTypesHolder.get(categoryName, substanceName);

        if (toSet == null)
            throw new MapModificationError("A substance or category is not present.\n" +
                                           "Category name: " + categoryName + ".\n" +
                                           "Substance name: " + substanceName + '.');

        set(toSet, x, y);
    }

    /**
     * Replaces a particle on given position with new instance of a substance
     * of a given class.
     *
     * Horizontal coordinate have to be value in range {@code <0, map width - 1>}.
     * Vertical coordinate have to be value in range {@code <0, map height - 1>}.
     *
     * @param x horizontal coordinate of the particle.
     * @param y vertical coordinate of the particle.
     */

    public void set(Class<?> substanceClass, int x, int y) {
        try {
            set(SubstanceBuilder.build(substanceClass), x, y);
        } catch (Exception e) {
            throw new MapModificationError(e);
        }
    }

    /**
     * Replaces a particle on given position with a given substance.
     *
     * Horizontal coordinate have to be value in range {@code <0, map width - 1>}.
     * Vertical coordinate have to be value in range {@code <0, map height - 1>}.
     *
     * @param x horizontal coordinate of the particle.
     * @param y vertical coordinate of the particle.
     *
     * @deprecated can cause a situation where one instance of the same particle is
     * in two different places, please use {@link GameMap#set(String, String, int, int)}
     * or {@link GameMap#set(Class, int, int)} instead.
     */

    public void set(SubstanceBase toSet, int x, int y) {
        checkCoordinates(x, y);

        if (toSet == null)
            throw new MapModificationError("Given substance object is null.");

        fields[x][y] = toSet;
    }

    private void checkCoordinates(int x, int y) {
        if (x < 0 || x >= width)
            throw new MapModificationError("Horizontal coordinate " + x + " is incorrect.");
        if (y < 0 || y >= height)
            throw new MapModificationError("Vertical coordinate " + y + " is incorrect.");
    }

    /**
     * A proxy class that is used to give a particle enough
     * information to react with other particles or to check
     * itself.
     *
     * @see SubstanceBase#checkSelf(Particle)
     * @see SubstanceBase#reactWith(Particle, SubstanceBase.Direction)
     */

    public class Particle {
        private Coordinates coordinates;
        private SubstanceBase field;

        /**
         * Constructor to be used only by {@link GameMap} object
         * (that's the reason for it to be protected).
         */

        protected Particle(SubstanceBase field, int x, int y) {
            this.field = field;
            coordinates = new Coordinates(x, y);
        }

        /**
         * @return a width of the map this particle is placed in.
         */

        public int getMapWidth() {
            return width;
        }

        /**
         * @return a height of the map this particle is placed in.
         */

        public int getMapHeight() {
            return height;
        }

        /**
         * @return a substance object of this particle.
         */

        public SubstanceBase asSubstance() {
            return field;
        }

        /**
         * @return current coordinates of this particle.
         *
         * @see Particle#swapWith(Particle)
         * @see Particle#swapWith(int, int)
         */

        public Coordinates getCoordinates() {
            return coordinates;
        }

        /**
         * Changes this particle to a particle with a given name
         * that have no category (or rather an empty one - "").
         *
         * A change will be made in a map object as well.
         *
         * @return a new particle that was created in place of this one.
         *
         * @see GameMap#set(String, String, int, int)
         */

        public Particle setTo(String substanceName) {
            setTo(substanceName, "");
            return getParticle(coordinates.x, coordinates.y);
        }

        /**
         * Changes this particle to a particle with a given name
         * and in given category.
         *
         * A change will be made in a map object as well.
         *
         * @return a new particle that was created in place of this one.
         *
         * @see GameMap#set(String, String, int, int)
         */

        public Particle setTo(String substanceName, String categoryName) {
            set(substanceName, categoryName, coordinates.x, coordinates.y);
            return getParticle(coordinates.x, coordinates.y);
        }

        /**
         * Swaps positions of this particle and the other one.
         * Coordinates of this particle will be updated.
         *
         * A change will be made in a map object as well.
         *
         * @see GameMap#swapFields(int, int, int, int)
         */

        public void swapWith(Particle particle) {
            swapWith(particle.coordinates.x, particle.coordinates.y);
        }

        /**
         * Swaps positions of this particle and one with given coordinates.
         * Coordinates of this particle will be updated.
         *
         * A change will be made in a map object as well.
         *
         * @see GameMap#swapFields(int, int, int, int)
         */

        public void swapWith(int x, int y) {
            swapFields(coordinates.x, coordinates.y, x, y);
            coordinates.x = x;
            coordinates.y = y;
        }

        /**
         * A data structore used to hold particle position information.
         */

        public class Coordinates {

            /**
             * Vertical position of a particle.
             * A value in range {@code <0, map width - 1>}.
             */

            public int x;

            /**
             * Vertical position of a particle.
             * A value in range {@code <0, map height - 1>}.
             */

            public int y;

            /**
             * Constructor to be used only by {@link Particle} object
             * (that's the reason for it to be protected).
             */

            protected Coordinates(int x, int y) {
                this.x = y;
                this.y = y;
            }
        }
    }

    /**
     * Exception that is thrown when map modification was made improperly.
     */

    public static class MapModificationError extends RuntimeException {
        private static String message = "Unable to properly modify map.";

        protected MapModificationError(Throwable e) {
            super(message, e);
        }

        protected MapModificationError(String additionalMessage) {
            super(message + '\n' + additionalMessage);
        }
    }
}

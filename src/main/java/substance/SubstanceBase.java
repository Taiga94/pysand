package substance;

import map.GameMap;
import substance.utils.SubstanceTypesHolder;

import java.util.List;

/**
 * A base class for every substance type.
 */

public abstract class SubstanceBase {
    /**
     * This method is used by the game only once,
     * when substance classes holder is being
     * created.
     *
     * Substance name and category name should be
     * static-like, so those should not change
     * at runtime.
     *
     * @see SubstanceBase#getCategoryName()
     * @see SubstanceTypesHolder
     */

    public abstract String getName();

    /**
     * Substances are categorized in the menu,
     * therefore a substance class should provide a
     * category that it should be placed into.
     *
     * If this method returns an empty string, then
     * substance will not be placed in any category.
     *
     * This method is used by the game only once,
     * when substance classes holder is being
     * created.
     *
     * Substance name and category name should be
     * static-like, so those should not change
     * at runtime.
     *
     * @see SubstanceBase#getName()
     * @see SubstanceTypesHolder
     */

    public abstract String getCategoryName();

    public abstract String getDescription();

    /**
     * @return a value between 0 and 1; defines red hue of substances stylize.
     */

    public abstract float getRedHue();

    /**
     * @return a value between 0 and 1; defines green hue of substances stylize.
     */

    public abstract float getGreenHue();

    /**
     * @return a value between 0 and 1; defines blue hue of substances stylize.
     */

    public abstract float getBlueHue();

    /**
     * @return a density of single substance particle, which is used to determine
     *         if a particle should float or sink.
     */

    public abstract float getDensity();

    /**
     * If a field is static, then gravity doesn't affect it.
     */

    public abstract boolean isStatic();

    /**
     * @return a thermal conductivity value of substance, it determines how fast
     *         the substance particles are radiating heat or accepting heat from
     *         other particles.
     */

    public abstract float getThermalConductivity();

    /**
     * @return the friction factor of a substance, a value between 0 and 1;
     *         the higher it is, the less probability that its particles
     *         would move horizontally.
     */

    public abstract float getFriction();

    /**
     * It is always called before any reaction, so the particle can set up
     * its current properties and react on it's own way with other
     * particles that are surrounding it.
     *
     * Surrounding particles amount may vary between 0 and 8.
     *
     * @param surroundingParticles to react with.
     */

    public abstract void react(GameMap.Particle selfAsParticle, List<GameMap.Particle> surroundingParticles);

    private boolean reacted;

    /**
     * Can be used to determine if given particle already reacted with
     * other particles.
     *
     * @return a flag of reaction.
     */

    public final boolean doReacted() {
        return this.reacted;
    }

    /**
     * Sets the flag of reaction to given value.
     *
     * @return this object.
     *
     * @deprecated only to be used by the game algorithm, please do
     * not use this method in any python implementation of
     * SubstanceBase class, it will cause undefined behaviour.
     */

    public final SubstanceBase setReacted(boolean reacted) {
        this.reacted = reacted;
        return this;
    }

    private int temperature;

    public final int getTemperature() {
        return this.temperature;
    }

    public final SubstanceBase setTemperature(int temperature) {
        this.temperature = temperature;
        return this;
    }

    public final SubstanceBase incrementTemperature() {
        this.temperature = this.temperature + 1;
        return this;
    }

    public final SubstanceBase decrementTemperature() {
        this.temperature = this.temperature - 1;
        return this;
    }
}

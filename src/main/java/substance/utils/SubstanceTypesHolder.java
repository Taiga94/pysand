package substance.utils;

import substance.SubstanceBase;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Static class holding all substances from a given set.
 * Substances are categorized using {@link SubstanceBase#getCategoryName()}.
 * When substance types are loaded to it there have to be created
 * instances of them.
 */

public class SubstanceTypesHolder {
    private static SubstanceTypesHolder instance;
    private static HashMap<String, HashMap<String, SubstanceTypeInfo>> substanceTypes;

    static {
        substanceTypes = new HashMap<String, HashMap<String, SubstanceTypeInfo>>();
    }

    private static String currentCategoryName;
    private static String currentSubstanceName;
    private static String currentSubstanceDescription;

    /**
     * Adds given substance type to a collection.
     * At least one class object have to be instantiated during the process.
     * It is done using {@link SubstanceBuilder#build(Class)} method.
     *
     * @param substanceClass to be added.
     */

    public static void add(Class<?> substanceClass) {
        initializeSubstanceInfo(substanceClass);

        if (isCurrentCategoryNotPresent())
            addCategory();

        HashMap<String, SubstanceTypeInfo> category = substanceTypes.get(currentCategoryName);

        if (category.containsKey(currentSubstanceName))
            throw new SubstanceAddingError();

        else
            category.put(currentSubstanceName, new SubstanceTypeInfo(substanceClass, currentSubstanceDescription));
    }

    /**
     * @return a type specified by parameters or null if the type is not present.
     */

    public static Class<?> get(String categoryName, String substanceName) {
        if (substanceTypes.containsKey(categoryName))
            return substanceTypes.get(categoryName).get(substanceName).type;
        else
            return null;
    }

    /**
     * @return a description of substance specified by parameters or null if the substance type is not present.
     */

    public static String getDescription(String categoryName, String substanceName) {
        if (substanceTypes.containsKey(categoryName))
            return substanceTypes.get(categoryName).get(substanceName).description;
        else
            return null;
    }

    /**
     * Puts all of the category and substance names in a list that can be used
     * to build a menu.
     *
     * @return a list of {@link Category} (if there are none, the list will be empty).
     */

    public static List<Category> getCategories() {
        List<Category> categories = new LinkedList<Category>();

        for (Map.Entry<String, HashMap<String, SubstanceTypeInfo>> categoryAsMap : substanceTypes.entrySet())
            categories.add(getCategory(categoryAsMap.getKey()));

        return categories;
    }

    private static void initializeSubstanceInfo(Class<?> substanceClass) {
        try {
            SubstanceBase substance = SubstanceBuilder.build(substanceClass);
            currentCategoryName = substance.getCategoryName();
            currentSubstanceName = substance.getName();
            currentSubstanceDescription = substance.getDescription();
        } catch (Exception e) {
            throw new SubstanceInfoObtainingError(e);
        }
    }

    /**
     * @return a category with given name or null if category is not present.
     */

    public static Category getCategory(String categoryName) {
        currentCategoryName = categoryName;

        if (isCurrentCategoryNotPresent())
            return null;

        HashMap<String, SubstanceTypeInfo> categoryMap = substanceTypes.get(categoryName);

        Category category = new Category(categoryName);

        for (Map.Entry<String, SubstanceTypeInfo> substance : categoryMap.entrySet())
            category.add(substance.getKey());

        return category;
    }

    private static boolean isCurrentCategoryNotPresent() {
        return (!substanceTypes.containsKey(currentCategoryName));
    }

    private static void addCategory() {
        substanceTypes.put(currentCategoryName, new HashMap<String, SubstanceTypeInfo>());
    }

    /**
     * Proxy class that allows to iterate through substance names in a given category.
     */

    public static class Category extends LinkedList<String> {
        private String name;

        /**
         * This method is protected so only SubstanceHolder class can
         * create an object of it.
         *
         * @param name of the category.
         */

        protected Category(String name) {
            super();
            this.name = name;
        }

        /**
         * @return a name of this category.
         */

        public String getName() {
            return name;
        }

        /**
         * @param name of the substance.
         * @return a substance type in this category or null if substance with given name is not present.
         */

        public Class<?> get(String name) {
            return SubstanceTypesHolder.get(this.name, name);
        }

        /**
         * @param name of the substance.
         * @return a description of substance in this category or null if substance with given name is not present.
         */

        public String getDescription(String name) {
            return SubstanceTypesHolder.getDescription(this.name, name);
        }
    }

    /**
     * Exception that is thrown when a category, substance name or it's description cannot be obtained.
     */

    public static class SubstanceInfoObtainingError extends RuntimeException {
        private static String message = "Unable to add substance type to holder, cannot obtain substance info.";

        protected SubstanceInfoObtainingError(Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Exception that is thrown when a substance cannot be added to holder.
     */

    public static class SubstanceAddingError extends RuntimeException {
        private static String message = "Unable to add substance type to holder, because it's already there.";

        protected SubstanceAddingError() {
            super(message);
        }
    }

    public static class SubstanceTypeInfo {
        public Class<?> type;
        public String description;

        public SubstanceTypeInfo(Class<?> type, String description) {
            this.type = type;
            this.description = description;
        }
    }
}

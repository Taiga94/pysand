package substance.utils;

import substance.SubstanceBase;

/**
 * All functionality of this class is located in method {@link SubstanceBuilder#build(Class)}.
 */

public class SubstanceBuilder {

    /**
     * Tries to create a new instance of a class derived from {@link SubstanceBase}.
     * To get a class from a python script, use {@link SubstanceClassGetter#get(String)}.
     *
     * @param clazz type defining the class.
     * @return new instance of {@link SubstanceBase}.
     *
     * @see SubstanceBase
     * @see SubstanceClassGetter
     */

    public static SubstanceBase build(Class<?> clazz) {
        SubstanceBase substance;

        try {
            substance = (SubstanceBase)clazz.getConstructor().newInstance();
        } catch (Exception e) {
            throw new SubstanceBuildingError(e);
        }

        return substance;
    }

    /**
     * Exception that is thrown when builder fails to create a substance object.
     */

    public static class SubstanceBuildingError extends RuntimeException {
        private static String message = "Attempt to build substance object resulted in exception.";

        protected SubstanceBuildingError(Throwable cause) {
            super(message, cause);
        }
    }
}

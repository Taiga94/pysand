package substance.utils;

import org.apache.commons.io.FilenameUtils;
import org.python.core.PyObject;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;
import substance.SubstanceBase;

import java.nio.file.Paths;

/**
 * A class that can be used to obtain class types of substances from python scripts.
 * All functionality is provided by {@link SubstanceClassGetter#get(String)} method.
 */

public class SubstanceClassGetter {
    private static PythonInterpreter interpreter = new PythonInterpreter(null, new PySystemState());

    /**
     * Tries to obtain class type from python script.
     * Target class should derive from {@link SubstanceBase}, and its
     * name should be the same as python script file name.
     *
     * @param path to script location
     * @return class type describing class defined in script
     *
     * @see SubstanceBase
     */

    public static Class<?> get(String path) {
        String className = getFileNameWithoutExtensionFromPath(path);

        return get(path, className);
    }

    private static Class<?> get(String path, String className) {
        interpreter.execfile(path);

        try {
            PyObject object = interpreter.get(className);
            return (Class<?>) object.__tojava__(Class.class);
        } catch (Exception e) {
            throw new ClassGettingError(e);
        }
    }

    private static String getFileNameWithoutExtensionFromPath(String path) {
        String fileName = Paths.get(path).getFileName().toString();

        return FilenameUtils.removeExtension(fileName);
    }

    /**
     * Exception that is thrown when a substance class cannot be obtained from a python script.
     */

    public static class ClassGettingError extends RuntimeException {
        private static String message = "Attempt to get substance class from script resulted in exception.";

        protected ClassGettingError(Throwable cause) {
            super(message, cause);
        }
    }
}

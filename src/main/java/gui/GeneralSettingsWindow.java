package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GeneralSettingsWindow extends JFrame implements ActionListener {

    private static final String windowTittle = "File selection";
    private String selectedFileName;

    public GeneralSettingsWindow() {
        super(windowTittle);

        setDefaultSettings();
        JButton fileSelectionButton = new JButton();
        fileSelectionButton.addActionListener(this);
        add(fileSelectionButton);
    }

    private void setDefaultSettings() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(100, 100);
    }

    public void openDialogWindow() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose a file");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.showOpenDialog(new JFrame());
        this.getContentPane().add(fileChooser);
        fileChooser.setVisible(true);
        System.err.println(fileChooser.getSelectedFile().getPath());
    }

    public void actionPerformed(ActionEvent e) {
        openDialogWindow();
    }
}

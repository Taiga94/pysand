package gui;

import substance.SubstanceBase;
import substance.utils.SubstanceBuilder;
import substance.utils.SubstanceTypesHolder;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

/**
 * A window class that allows choosing from substances
 * contained in {@link SubstanceTypesHolder}.
 */

public class SubstanceTypeSelectorWindow extends JFrame implements TreeSelectionListener {
    private static final String WINDOW_TITTLE = "Substance selection";
    private static final String ROOT_NAME = "Categories";

    private String selectedCategoryName;
    private String selectedSubstanceName;

    private JTree categoryTree;
    JTextArea descriptionTextArea;

    /**
     * Creates a window, adds listener and builds
     * substance categories tree.
     */

    public SubstanceTypeSelectorWindow() {
        super(WINDOW_TITTLE);

        setDefaultSettings();

        setLayout(new GridLayout(2, 0));

        createCategoryTree();
        createDescriptionTextField();
    }

    private void setDefaultSettings() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(150, 200);
        setMinimumSize(new Dimension(150, 200));
    }

    private void createCategoryTree() {
        categoryTree = new JTree(getRoot());
        GuiStylizer.stylize(categoryTree);
        categoryTree.addTreeSelectionListener(this);
        categoryTree.setVisible(true);

        add(categoryTree);
    }

    private void createDescriptionTextField() {
        descriptionTextArea = new JTextArea();
        GuiStylizer.stylize(descriptionTextArea);
        descriptionTextArea.setVisible(true);
        descriptionTextArea.setText("");
        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setWrapStyleWord(true);
        descriptionTextArea.setEditable(false);

        add(descriptionTextArea);
    }

    private DefaultMutableTreeNode getRoot() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(ROOT_NAME);

        for(SubstanceTypesHolder.Category category : SubstanceTypesHolder.getCategories()) {
            DefaultMutableTreeNode currentCategoryNode = new DefaultMutableTreeNode(category.getName());

            for(String substanceName : category)
                currentCategoryNode.add(new DefaultMutableTreeNode(substanceName));

            root.add(currentCategoryNode);
        }

        return root;
    }

    /**
     * @return currently selected substance category name.
     */

    public String getSelectedCategoryName() {
        return selectedCategoryName;
    }

    /**
     * @return currently selected substance name.
     */

    public String getSelectedSubstanceName() {
        return selectedSubstanceName;
    }

    /**
     * Creates a SubstanceBase object using {@link SubstanceBuilder}
     * with a class obtained from {@link SubstanceTypesHolder}.
     */

    public SubstanceBase getInstanceOfSelectedSubstance() {
        return SubstanceBuilder.build(SubstanceTypesHolder.get(selectedCategoryName, selectedSubstanceName));
    }

    /**
     * Listener method that is invoked whenever a substance
     * in a category is selected.
     * If a category is selected instead or a tree root,
     * nothing changes.
     */

    public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
        Object[] path = treeSelectionEvent.getPath().getPath();
        if (path.length == 3) {
            selectedCategoryName = path[1].toString();
            selectedSubstanceName = path[2].toString();

            descriptionTextArea.setText(
                SubstanceTypesHolder.getDescription(selectedCategoryName, selectedSubstanceName)
            );
        }
    }
}

package gui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class GuiStylizer {
    public static final Color BACKGROUND_COLOR = new Color(23, 23, 23);
    public static final Color BACKGROUND_SELECTION_COLOR = new Color(21, 60, 60);
    public static final Color FOREGROUND_COLOR = new Color(191, 191, 191);
    public static final Color FOREGROUND_SELECTION_COLOR = new Color(191, 50, 61);
    public static final Color BORDER_COLOR = new Color(39, 39, 39);

    public static final Border BORDER = new LineBorder(BORDER_COLOR, 3);
    public static final Border NO_BORDER = new LineBorder(BACKGROUND_COLOR, 3);

    public static void stylize(JComponent component) {
        component.setBackground(BACKGROUND_COLOR);
        component.setForeground(FOREGROUND_COLOR);
    }

    public static void stylize(JTextArea textArea) {
        stylize((JComponent)textArea);
        textArea.setBorder(BORDER);
        textArea.setSelectionColor(BACKGROUND_SELECTION_COLOR);
        textArea.setSelectedTextColor(FOREGROUND_SELECTION_COLOR);
    }

    public static void stylize(JTree tree) {
        stylize((JComponent)tree);

        tree.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree,
                                                          Object value,
                                                          boolean sel,
                                                          boolean expanded,
                                                          boolean leaf,
                                                          int row,
                                                          boolean hasFocus) {

                String text = value.toString();

                if (sel) {
                    setForeground(FOREGROUND_SELECTION_COLOR);
                    setBackground(BACKGROUND_SELECTION_COLOR);
                    setBackgroundNonSelectionColor(BACKGROUND_SELECTION_COLOR);
                    setBorder(BORDER);
                }

                else {
                    setForeground(FOREGROUND_COLOR);
                    setBackground(BACKGROUND_COLOR);
                    setBackgroundNonSelectionColor(BACKGROUND_COLOR);
                    setBorder(NO_BORDER);
                }
                setText(text);

                return this;
            }
        });
    }
}

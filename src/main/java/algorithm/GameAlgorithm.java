package algorithm;

import map.GameMap;
import substance.SubstanceBase;

import java.util.List;

/**
 * {@link GameAlgorithm#iterateSimulation(GameMap)}
 */

public class GameAlgorithm {
    private static int fieldOffset;
    private static boolean makeReaction = false;
    private static boolean particleMoved;
    private static float thermalConductivity;
    private static final int temperatureTransferRate = 2000;

    /**
     * Iterates through the entire map and simulates things like gravity,
     * thermal conductivity and substance slippery.
     * Also lets every substance react with those around it.
     */

    public static void iterateSimulation(GameMap map) {
        for (int x = 0; x < map.getWidth(); ++x)
            for (int y = 0; y < map.getHeight(); ++y)
                if (didNotReactYet(map.get(x, y)))
                    simulateParticle(map, x, y);

        revertMakeReactionVariable();
    }

    private static boolean didNotReactYet(SubstanceBase substance) {
        return (makeReaction != substance.doReacted());
    }

    private static void revertMakeReactionVariable() {
        if (makeReaction)
            makeReaction = false;
        else
            makeReaction = true;
    }

    private static void simulateParticle(GameMap map, int x, int y) {
        SubstanceBase currentField = map.get(x, y);

        List<GameMap.Particle> fieldsAround = map.getParticlesAround(x, y);

        currentField.setReacted(makeReaction);
        currentField.react(map.getParticle(x, y), fieldsAround);

        particleMoved = false;

        if (isFieldInfluencedByGravity(map, currentField, y))
            simulateGravity(map, currentField, x, y);

        if (!particleMoved && isFieldSlippery(currentField))
            slipFieldInRandomDirection(map, x, y);

        simulateTemperatureConduction(currentField, fieldsAround);
    }

    private static boolean isFieldInfluencedByGravity(GameMap map, SubstanceBase field, int y) {
        return (y < map.getHeight() - 1 && !field.isStatic());
    }

    private static void simulateGravity(GameMap map, SubstanceBase currentField, int x, int y) {
        SubstanceBase fieldBelow = map.get(x, y + 1);

        if (canFieldsBeSwappedBecauseOfGravity(currentField, fieldBelow)) {
            map.swapFields(x, y, x, y + 1);
            particleMoved = true;
        }

        else {
            fieldOffset = getRandomFieldOffset();
            if (canBeAffectedByGravityDiagonally(x, map.getWidth())) {
                fieldBelow = map.get(x + fieldOffset, y + 1);

                if (canFieldsBeSwappedBecauseOfGravity(currentField, fieldBelow)) {
                    map.swapFields(x, y, x + fieldOffset, y + 1);
                    particleMoved = true;
                }

            }
        }
    }

    private static boolean canFieldsBeSwappedBecauseOfGravity(SubstanceBase currentField, SubstanceBase fieldBelow) {
        return (!(fieldBelow.isStatic()) && makeReaction != fieldBelow.doReacted() &&
                (currentField.getDensity() > fieldBelow.getDensity() ||
                (currentField.getDensity() == fieldBelow.getDensity() && currentField.getTemperature() < fieldBelow.getTemperature())));
    }

    private static int getRandomFieldOffset() {
        // TODO: provide proper implementation. This method should return a random value, -1 or 0.
        return 1;
    }

    private static boolean canBeAffectedByGravityDiagonally(int x, int mapWidth) {
        return ((fieldOffset == (-1) && x > 0) || fieldOffset == 1 && x < mapWidth - 1);
    }

    private static boolean isFieldSlippery(SubstanceBase currentField) {
        // TODO: implement random slippery algorithm that uses friction to determine if field is slippery at the moment.
        return (currentField.getFriction() < 1.0);
    }

    private static void slipFieldInRandomDirection(GameMap map, int x, int y) {
        fieldOffset = getRandomFieldOffset();

        SubstanceBase fieldBeside;

        fieldBeside = map.get(x + fieldOffset, y);

        if (fieldBeside == null)
            return;

        if (isFieldSlippery(fieldBeside) && makeReaction != fieldBeside.doReacted())
            map.swapFields(x, y, x + fieldOffset, y);
    }

    private static void simulateTemperatureConduction(SubstanceBase currentField, List<GameMap.Particle> fieldsAround) {
        if (currentField.getThermalConductivity() <= 0)
            return;

        for (GameMap.Particle fieldAround : fieldsAround) {
            SubstanceBase field = fieldAround.asSubstance();

            if (field.getThermalConductivity() <= 0)
                continue;

            thermalConductivity = calculateThermalConductivityBetween(field, currentField);

            if (!doRandomilyTransferTemperature())
                continue;

            if (field.getTemperature() > currentField.getTemperature() && field.getTemperature() - 1 >= 0) {
                field.decrementTemperature();
                currentField.incrementTemperature();
            }

            else if (field.getTemperature() < currentField.getTemperature() && currentField.getTemperature() - 1 >= 0) {
                field.incrementTemperature();
                currentField.decrementTemperature();
            }
        }
    }

    private static float calculateThermalConductivityBetween(SubstanceBase a, SubstanceBase b) {
        int temperatureDifference = Math.abs(a.getTemperature() - b.getTemperature());
        return ((float) (temperatureDifference * ((a.getThermalConductivity() + b.getThermalConductivity()) / 2.5))); // PODZIEL PRZEZ 3
    }

    private static boolean doRandomilyTransferTemperature() {
        // TODO: Zmienić na javę: return (rand() % temperatureTransferRate < thermalConductivity);
        //return (temperatureTransferRate < thermalConductivity);
        return true;
    }
}
